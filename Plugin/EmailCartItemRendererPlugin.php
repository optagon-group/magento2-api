<?php
namespace CustomSumit\Api\Plugin;

class EmailCartItemRendererPlugin {
	public function __construct(\Psr\Log\LoggerInterface $log) {
		$this->log = $log;
	}
	public function aroundGetItemOptions(\Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder $subject, \Closure $proceed) {
		$options = $proceed();
		foreach ($options as $k => $option) {
			if (! empty ( $option ['print_value'] ) && (! empty ( $option ['label'] ) && in_array ( $option ['label'], [ 
				'Kit reference',
				'Kit size' 
			] ))) {
				$options[$k]['value'] = $option ['print_value'];
			}
			
		}
		return $options;
	}

}
?>