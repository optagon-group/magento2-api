<?php
namespace CustomSumit\Api\Plugin;

class CheckoutCartItemRendererPlugin {
	public function __construct(\Psr\Log\LoggerInterface $log) {
		$this->log = $log;
	}
	public function aroundGetFormattedOptionValue(\Magento\Catalog\Helper\Product\Configuration $subject, \Closure $proceed, $optionValue, $params) {

		// Allow markup in the cart view page
		if (! empty ( $optionValue ['print_value'] ) && (! empty ( $optionValue ['label'] ) && in_array ( $optionValue ['label'], [
			'Kit reference',
			'Kit size'
		] ))) {
			$optionValue ['value'] = $optionValue ['print_value'];
		}
		$returnValue = $proceed ( $optionValue );
		return $returnValue;
	}
}
?>
