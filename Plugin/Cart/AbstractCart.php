<?php
namespace CustomSumit\Api\Plugin\Cart;

use Magento\Checkout\Block\Cart\AbstractCart as MagentoAbstractCart;

class AbstractCart
{
    const CUSTOMSUMIT_TEMPLATE_PATH = 'CustomSumit_Api::cart/item/default.phtml';
    
    /**
     * Override default cart item renderer template.
     *
     * @param MagentoAbstractCart $subject
     * @param $result
     * @return mixed
     */
    public function afterGetItemRenderer(MagentoAbstractCart $subject, $result)
    {
        $result->setTemplate(self::CUSTOMSUMIT_TEMPLATE_PATH);
        
        return $result;
    }
}
