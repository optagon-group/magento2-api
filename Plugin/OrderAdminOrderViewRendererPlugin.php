<?php
namespace CustomSumit\Api\Plugin;

class OrderAdminOrderViewRendererPlugin {
        public function __construct(\Psr\Log\LoggerInterface $log) {
                $this->log = $log;
        }
        public function aroundGetFormattedOption(\Magento\Sales\Block\Adminhtml\Items\Column\Name $subject, \Closure $proceed, $value) {
                if (strpos($value, 'm&lt;sup&gt;2&lt;/sup&gt;') !== false || strpos($value, 'View Kit&lt;/a&gt;') !== false) {
                        $value = html_entity_decode($value);
                        $returnValue = ['value' => nl2br($value)];
                } else {
                        $returnValue = $proceed ( $value );
                }
                return $returnValue;
        }
}
?>
