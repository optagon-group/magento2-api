<?php
namespace CustomSumit\Api\Plugin;

class OrderCartItemRendererPlugin {
	public function __construct(\Psr\Log\LoggerInterface $log) {
		$this->log = $log;
	}
	public function aroundGetFormatedOptionValue(\Magento\Sales\Block\Order\Item\Renderer\DefaultRenderer\Interceptor $subject, \Closure $proceed, $optionValue) {

		// Allow markup in the cart view page
		if (! empty ( $optionValue ['print_value'] ) && (! empty ( $optionValue ['label'] ) && in_array ( $optionValue ['label'], [
			'Kit reference',
			'Kit size'
		] ))) {
			$optionValue ['value'] = $optionValue ['print_value'];
		}
		$returnValue = $proceed ( $optionValue );
		return $returnValue;
	}
}
?>
