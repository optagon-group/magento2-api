<?php
namespace CustomSumit\Api\Plugin;

class CartItemRendererPlugin {
	public function __construct(\Psr\Log\LoggerInterface $log, \CustomSumit\Api\Helper\Data $helper) {
		$this->log = $log;
    $this->helper = $helper;
	}
  public function afterGetProductOptions(\Magento\Checkout\Block\Cart\Item\Renderer $subject, $result){
    return $this->helper->formatMembraneCustomOptions($result, $subject->getQty());
  }
	public function aroundGetFormatedOptionValue(\Magento\Checkout\Block\Cart\Item\Renderer $subject, \Closure $proceed, $optionValue = null) {
		// customise the cart page markup for the membrane product
		if (!empty($optionValue['print_value']) &&
       (!empty($optionValue['label']) &&
       in_array($optionValue ['label'], [
         'Kit reference',
         'Kit size'
        ]
      ))) {
      $optionValue['value'] = $optionValue['print_value'];
		}
		$returnValue = $proceed($optionValue);

		return $returnValue;
	}
}
?>
