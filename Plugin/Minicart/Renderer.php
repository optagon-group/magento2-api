<?php

namespace CustomSumit\Api\Plugin\Minicart;

class Renderer
{
  public function __construct(\Psr\Log\LoggerInterface $log, \Magento\Catalog\Model\ProductRepository $productRepository) {
		$this->log = $log;
    $this->productRepository = $productRepository;
	}

	public function aroundGetItemData($subject, $proceed, $item)
	{
    $result = $proceed($item);
    $product = $this->productRepository->getById($result['product_id']);
		if($product->getIsMembrane()){
			$i = 0;
			foreach($result['options'] as $option){
				// loop through the options and find the EPDM Width and remove any qty text prefix (e.g. '12 x 5.42 m')
				if($option['label'] == 'EPDM Width'){
					$outputValue = $option['print_value'];
					$outputValue = preg_replace('/\d*[.]*\d*\s[x]\s/', '', $outputValue, 1);
					$option['print_value'] = $outputValue;
				}
			}
			// set the new Width print_value without the qty prefix
			$result['options'][$i]['print_value'] = $outputValue;
			$result['product_is_membrane'] = 1;
		}else{
      $result['product_is_membrane'] = 0;
    }

		return $result;
	}
}
