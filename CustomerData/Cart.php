<?php
namespace CustomSumit\Api\CustomerData;

use Magento\Catalog\Block\ShortcutButtons;
use Magento\Catalog\Model\ProductRepository as MagentoProductRepository;
use Magento\Catalog\Model\ResourceModel\Url as MagentoUrlResourceModel;
use Magento\Checkout\CustomerData\Cart as MagentoCustomerCartData;
use Magento\Checkout\CustomerData\ItemPoolInterface;
use Magento\Checkout\Helper\Data as MagentoCheckoutDataHelper;
use Magento\Checkout\Model\Cart as MagentoCartModel;
use Magento\Checkout\Model\Session;
use Magento\Framework\DataObject;
use Magento\Framework\View\LayoutInterface;
use Magento\Quote\Model\Quote\Item;

class Cart extends MagentoCustomerCartData
{
    /** @var MagentoProductRepository */
    private $productRepository;

    /**
     * Cart constructor.
     *
     * @param Session $checkoutSession
     * @param MagentoUrlResourceModel $catalogUrl
     * @param MagentoCartModel $checkoutCart
     * @param MagentoCheckoutDataHelper $checkoutHelper
     * @param ItemPoolInterface $itemPoolInterface
     * @param LayoutInterface $layout
     * @param MagentoProductRepository $productRepository
     * @param array $data
     */
    public function __construct(
        Session $checkoutSession,
        MagentoUrlResourceModel $catalogUrl,
        MagentoCartModel $checkoutCart,
        MagentoCheckoutDataHelper $checkoutHelper,
        ItemPoolInterface $itemPoolInterface,
        LayoutInterface $layout,
        MagentoProductRepository $productRepository,
        array $data = []
    ) {
        $this->productRepository = $productRepository;

        parent::__construct(
            $checkoutSession,
            $catalogUrl,
            $checkoutCart,
            $checkoutHelper,
            $itemPoolInterface,
            $layout,
            $data
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getSectionData()
    {
        $totals = $this->getQuote()->getTotals();
        $subtotalAmount = $totals['subtotal']->getValue();
        $affectedSummaryCount = 0;

        // loop through the basket items and look for the membrane product so we can fix the quantity
        foreach (array_reverse($this->getAllQuoteItems()) as $item) {
            $product = $item->getOptionByCode('product_type') !== null
                ? $item->getOptionByCode('product_type')->getProduct()
                : $item->getProduct();

            $_product = $this->productRepository->getById($product->getId());
            $basketItemQty = (int)$item->getQty();

            if ($_product->getIsMembrane()) {
                // fix the qty to output to 1 and not the Area sqm
                $affectedSummaryCount = $affectedSummaryCount + 1;
            } else {
                $affectedSummaryCount = $affectedSummaryCount + $basketItemQty;
            }
        }

        return [
            'affected_summary_count' => $affectedSummaryCount,
            'summary_count' => $this->getSummaryCount(),
            'subtotalAmount' => $subtotalAmount,
            'subtotal' => isset($totals['subtotal'])
                ? $this->checkoutHelper->formatPrice($subtotalAmount)
                : 0,
            'possible_onepage_checkout' => $this->isPossibleOnepageCheckout(),
            'items' => $this->getRecentItems(),
            'extra_actions' => $this->layout->createBlock(ShortcutButtons::class)->toHtml(),
            'isGuestCheckoutAllowed' => $this->isGuestCheckoutAllowed(),
            'website_id' => $this->getQuote()->getStore()->getWebsiteId(),
            'storeId' => $this->getQuote()->getStore()->getStoreId()
        ];
    }

    /**
     * Get array of last added items
     *
     * @return Item[]
     */
    protected function getRecentItems()
    {
        $items = [];

        if (!$this->getSummaryCount()) {
            return $items;
        }

        foreach (array_reverse($this->getAllQuoteItems()) as $item) {
            /* @var $item Item */
            if (!$item->getProduct()->isVisibleInSiteVisibility()) {
                $product = $item->getOptionByCode('product_type') !== null
                    ? $item->getOptionByCode('product_type')->getProduct()
                    : $item->getProduct();

                $products = $this->catalogUrl->getRewriteByProductStore([$product->getId() => $item->getStoreId()]);

                if (!isset($products[$product->getId()])) {
                    // check for a custom kit and add to minicart if found
                    $sku = $product->getSku();

                    if (strpos($sku, 'customkit_') > -1) {
                        $items[] = $this->itemPoolInterface->getItemData($item);
                    }

                    continue;
                }

                $urlDataObject = new DataObject($products[$product->getId()]);
                $item->getProduct()->setUrlDataObject($urlDataObject);
            }

            $items[] = $this->itemPoolInterface->getItemData($item);
        }

        return $items;
    }
}
