<?php
namespace CustomSumit\Api\Controller\Basket;

use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\NotFoundException;
use \CustomSumit\Adapter\Adapter;

/**
 * Add Action
 *
 * @author Andrew Male - Custom SumIT
 */
class Add extends \Magento\Framework\App\Action\Action {

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 * @param \CustomSumit\Api\Helper\Data $dataHelper
	 * @param \Magento\Checkout\Model\Cart $cart,
	 * @param \Psr\Log\LoggerInterface $log
	 */
	public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Catalog\Model\ProductRepository $productRepository, \CustomSumit\Api\Helper\Data $dataHelper, \Magento\Checkout\Helper\Cart $cartHelper, \Magento\Checkout\Model\Cart $cart, \Psr\Log\LoggerInterface $log) {
    parent::__construct($context);
		$this->log = $log;
		$this->dataHelper = $dataHelper;
		$this->cartHelper = $cartHelper;
		$this->cart = $cart;
		$this->productRepository = $productRepository;
	}

	/**
	 * Dispatch request
	 *
	 * @param RequestInterface $request
	 * @return \Magento\Framework\App\ResponseInterface
	 * @throws \Magento\Framework\Exception\NotFoundException
	 */
	public function dispatch(\Magento\Framework\App\RequestInterface $request) {
		if (! $this->dataHelper->isEnabled ()) {
			throw new NotFoundException(__('Page not found.'));
		}
		return parent::dispatch($request);
	}

	/**
	 * Execute view action
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function execute() {
		$addedToCart = false;
		$privateKey = $this->dataHelper->privateKey();

		// Check Params
		$params = $this->getRequest()->getParams();
		$requiredParams = array (
			'data'
		);
		foreach($requiredParams as $requiredParam){
			if(empty($params[$requiredParam]))
				throw new InputException(__('Required parameters missing'));
		}

		// Attempt to decrypt incoming Skyseal payload
		try {
			$Adapter = new Adapter($privateKey);
		} catch (\Exception $e) {
			$this->log->critical('Custom SumIT: ' . $e->getMessage());
      return false;
		}
		try {
      // if the kit_products array is passed then assume this is a Product Aware site
      if(isset($Adapter->data['kit_products'])){
        // add the products for product aware
        $addedToCart = $this->addProductAwareProducts($Adapter->data);
      }else{
        // Add Simple Product to Cart
        // Get the Product ID based on Kit System
  			$productId = $this->dataHelper->skysealProductId($Adapter->data ['kit_system']);

  			// The Markup and link to Kit Ref option
  			$productKitRefValue = sprintf ( '<a href="%s">%s</a>', $Adapter->data ['kit_url'], $Adapter->data ['kit_ref'] . ' - View Kit' );
  			$productKitSizeValue = $Adapter->data ['kit_size'] . 'm<sup>2</sup>';

  			// Find custom option ids
  			$product = $this->productRepository->getById ( $productId );
  			$productOptions = $product->getOptions ();
  			$options = array ();
  			foreach ( $productOptions as $productOption ) {
  				switch ($productOption->getTitle ()) {
  					case 'Kit reference' :
  						$options [$productOption->getId ()] = $productKitRefValue;
  						$kitRefProductOptionId = $productOption->getId ();
  						break;
  					case 'Kit size' :
  						$options [$productOption->getId ()] = $productKitSizeValue;
  						$kitSizeProductOptionId = $productOption->getId ();
  						break;
  				}
  			}

  			// Get the Magento Cart
  			$alreadyInCart = false;
  			foreach($this->cart->getQuote()->getAllVisibleItems() as $item){
  				if($item->getProductId() == $productId) {
  					foreach($item->getBuyRequest()->getOptions() as $code => $option){
  						if($kitRefProductOptionId == $code && $option == $productKitRefValue){
  							$alreadyInCart = true;
  							break;
  						}
  					}
  				}
  			}
  			// If there's not already a kit with this ref added, then add it
  			if(!$alreadyInCart){
  				// Add a product with custom options
  				$productParams = array (
  					'product' => $productId,
  					'qty' => 1,
  					'options' => $options
  				);
  				// Set Kit Price for Kit Price observer
  				$product->kitPrice = $Adapter->data['kit_price'];
  				$item = $this->cart->addProduct($product, $productParams);
  				// save the cart
          $this->cart->save();
  			}
  			$addedToCart = true;
      }
		} catch (Exception $e){
			$this->log->critical('Custom SumIT: ' . $e->getMessage());
			$addedToCart = false;
		}
		// send back the response to the app or just redirect to the checkout if no callback was provided
    if(empty($params['callback'])){
      $this->getResponse()->setRedirect($this->cartHelper->getCartUrl());
    }else{
      $Adapter->setReturnUrl($this->cartHelper->getCartUrl())->setAddedToCart($addedToCart)->respond();
    }
	}

  /**
	 * Updates the cart with all products for product aware merchants
	 *
	 * @return boolean indicating success for the call
	 */
	private function addProductAwareProducts($appData){
    $kitBuilderProducts = $appData['kit_products'];
    $kitBuilderKitRef = $appData['kit_ref'];
    if($this->checkForKitInCart($kitBuilderKitRef)){
      // kit has already been added to the cart so return true
      return true;
    }

    // loop through the products and try to add to the cart
    foreach($kitBuilderProducts as $kitBuilderProduct){
      $options = '';
      $params = array();
      $productSku = $kitBuilderProduct['sku'];
      $productParentSku = (isset($kitBuilderProduct['parent_sku']) ? $kitBuilderProduct['parent_sku'] : '');
      $productQty = $kitBuilderProduct['qty'];
      $isMembraneProduct = false;
      $options = null;
      $superAttributes = null;
      $mageProduct = null;

      // retrieve the magento product using the SKU or parent SKU
      if($productParentSku == ''){
        $mageProduct = $this->getProduct($productSku);
      }else{
        $mageProduct = $this->getProduct($productParentSku);
      }

      if(!$mageProduct){
        $this->log->critical("Custom SumIT: Unable to load Magento product {$productSku}/{$productParentSku}");
        return false;
      }
      $mageProductId = $mageProduct->getId();

      switch($mageProduct->getTypeId()){
        case 'configurable':
          // for configurable products we need to select the children product that matches the KB data product sku
          // get the children products for the parent
          $productAttributeOptions = $mageProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($mageProduct);
          $superAttributes = [];
          // get the child product as well (i.e. the actual product that is in the kit)
          $mageChildProduct = $this->getProduct($productSku);
          foreach($productAttributeOptions as $productAttributeOption){
            $superAttributes[$productAttributeOption['attribute_id']] =  $mageChildProduct->getData($productAttributeOption['attribute_code']);
          }
          break;

        case 'simple':
          // if we have a simple product, then we need to set the Custom Option on it to the Kit Builder data
          // first check for the 'width' and 'length' on the KB data, if present then we have the membrane product
          if(isset($kitBuilderProduct['length']) && isset($kitBuilderProduct['width'])){
            $isMembraneProduct = true;
            // now get the 'size' KB data and format to XXXX to use to select the custom option in Magento based on the SKU
            $kitBuilderMetaData = str_pad(preg_replace('/[^0-9^x]/', '', $kitBuilderProduct['size']), 4, "0", STR_PAD_LEFT);
            // loop through all custom option values on this product to find the selected option value
            $customOptions = $mageProduct->getOptions();
            // for the membrane product we shall get the options by name
            foreach($customOptions as $option){
              if(strpos($option->getTitle(), 'EPDM Length') > -1){
                $options[$option->getId()] = (string)$kitBuilderProduct['length'];
              }else if(strpos($option->getTitle(), 'EPDM Area') > -1){
                $area = (float)$kitBuilderProduct['length'] * (float)$kitBuilderProduct['width'];
                $area = number_format($area, 2);
                $options[$option->getId()] = (string)$area;
                $productQty = $area;
              }else if(strpos($option->getTitle(), 'EPDM Width') > -1){
                // loop through the 'EPDM Width' option values for this Custom Option and match the option sku to the KB meta data
                foreach ($option->getValues() as $optionValueId => $optionValue) {
                  if($optionValue->getSku() == $kitBuilderMetaData){
                    // found the matching SKU on the option for the the selected value
                    $options[$option->getId()] = (string) $optionValueId;
                  }
                }
              }else{
                foreach ($option->getValues() as $optionValueId => $optionValue){
                  $options[$option->getId()] = (string) $optionValueId;
                }
              }
            }
          }else{
            // other product can have 'size' and/or 'colour' attributes
            $sizeKitBuilderMetaData = (isset($kitBuilderProduct['size']) ? $kitBuilderProduct['size'] : '');
            $sizeKitBuilderMetaData = str_replace(['.'], '', $sizeKitBuilderMetaData);
            $sizeKitBuilderMetaData = strtolower($sizeKitBuilderMetaData);
            $colourKitBuilderMetaData = (isset($kitBuilderProduct['colour']) ? $kitBuilderProduct['colour'] : '');
            $colourKitBuilderMetaData = str_replace(['-', '_'], ' ', $colourKitBuilderMetaData);
            // get the magento custom options
            $customOptions = $mageProduct->getOptions();
            //echo $productSku . " \nsize: {$sizeKitBuilderMetaData} \ncolour: {$colourKitBuilderMetaData}\n\n";
            foreach($customOptions as $option){
              if($option->getValues()){
                // for Colour we are matching the custom option on Title
                // for Size we match on the SKU
                foreach ($option->getValues() as $optionValueId => $optionValue) {
                  // e.g. App: anthracite-grey -> M2: Anthracite Grey
                  $optionValueTitle = trim(strtolower($optionValue->getTitle()));
                  $optionValueSku = trim(strtolower($optionValue->getSku()));
                  // match the colour on the Title of the custom option
                  if($colourKitBuilderMetaData != '' && ($optionValueTitle == $colourKitBuilderMetaData)){
                    $options[$option->getId()] = (string) $optionValueId;
                    break;
                  }
                  if($colourKitBuilderMetaData != '' && ($optionValueSku == $colourKitBuilderMetaData)){
                    $options[$option->getId()] = (string) $optionValueId;
                    break;
                  }
                  // match the size from the app with the SKU on the custom option
                  if($sizeKitBuilderMetaData != '' && ($optionValueSku == $sizeKitBuilderMetaData)){
                    $options[$option->getId()] = (string) $optionValueId;
                    break;
                  }
                  // if there are no size or colour set we can also check for the actual SKU match
                  // e.g. on PG-C2 where the custom option has the SKU LH-Wall2Kerb
                  if($sizeKitBuilderMetaData == '' && $colourKitBuilderMetaData == ''){
                    if($optionValueSku == strtolower($productSku)){
                      $options[$option->getId()] = (string) $optionValueId;
                    }
                  }
                }
              }
            }
          }
          break;
        default:
      }

      // put it all together and add the magento product to the cart
      $mageProductParams = array(
        'product' => $mageProductId,
        'options' => $options,
        'qty' => $productQty
      );
      if($superAttributes !== null){
        // for configurable products
        $mageProductParams['super_attribute'] = $superAttributes;
      }
      if($kitBuilderProduct['qty'] > 1 && $isMembraneProduct){
        // we need to add multiple membrane products of the same config because the Qty field in Mage is used for Area
        $extraOptions = [
          'label' => 'Qty Required',
          'value' => $kitBuilderProduct['qty'],
          'print_value' => $kitBuilderProduct['qty']
        ];
        for($i = 0; $i < $kitBuilderProduct['qty']; $i++){
          $addedToCart = $this->addProductToCart($mageProduct, $mageProductParams, $kitBuilderKitRef, $extraOptions);
        }
      }else{
        $addedToCart = $this->addProductToCart($mageProduct, $mageProductParams, $kitBuilderKitRef);
      }
      if(!$addedToCart){
        return false;
      }
    }

    $this->cart->save();
    return true;
  }


  private function getProduct($sku){
    try {
      $this->productRepository->cleanCache();
      $product = $this->productRepository->get($sku);
      return $product;

    } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
      $this->log->critical("Custom SumIT: getProduct failed: {$sku} - " . $e->getMessage());
      return false;
    }
  }

  private function checkForKitInCart($kitReference){
    // check to see if the cart already has this kit added to it
    $kitIsInCart = false;
    foreach($this->cart->getQuote()->getAllVisibleItems() as $item){
      $additionalOptions = $item->getOptionByCode('additional_options');
      if(isset($additionalOptions)){
        $additionalOptions = \Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Serialize\Serializer\Json")->unserialize($additionalOptions->getValue());
        foreach($additionalOptions as $option){
          if($option['value'] == $kitReference){
            $kitIsInCart = true;
            break;
          }
        }
      }
    }
    return $kitIsInCart;
  }

  private function addProductToCart($magentoProduct, $params, $kitReference, $extraOptions = null){
    $request = new \Magento\Framework\DataObject();
    $request->setData($params);
    // add in the kit reference on each product
    $additionalOptions = null;
    $additionalOptions[] = array (
      'label' => 'Kit Reference',
      'value' => $kitReference,
      'print_value' => $kitReference
    );
    if($extraOptions !== null){
      $additionalOptions[] = $extraOptions;
    }
    $magentoProduct->addCustomOption('additional_options', \Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Serialize\Serializer\Json")->serialize($additionalOptions));
    try {
      // add this product to the cart
      $this->cart->addProduct($magentoProduct, $request);
      return true;
    } catch (\Exception $e){
      // something went wrong so log it
      $this->log->critical("Custom SumIT: addProductToCart: " . $magentoProduct->getSku() . " - " . $e->getMessage());
      return false;
    }
  }
}
