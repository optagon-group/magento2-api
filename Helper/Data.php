<?php

namespace CustomSumit\Api\Helper;

/**
 * CustomSumit API Data helper
 *
 * @author jherring
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {

	/**
	 * Path to store config if extension is enabled
	 *
	 * @var string
	 */
	const XML_PATH_ENABLED = 'customsumit_api/core/enabled';
	const XML_PATH_PRIVATE_KEY = 'customsumit_api/core/private_key';
	const XML_PATH_SKYSEAL_EPDM_PRODUCT = 'customsumit_api/kitbuilder/epdm_product_id';
	const XML_PATH_SKYSEAL_GRP_PRODUCT = 'customsumit_api/kitbuilder/grp_product_id';
	const XML_PATH_SKYSEAL_FLEXIGRP_PRODUCT = 'customsumit_api/kitbuilder/flexigrp_product_id';

	/**
	 * __construct
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 */
	public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Framework\ObjectManagerInterface $objectManager, \Psr\Log\LoggerInterface $log) {
		$this->scopeConfig = $scopeConfig;
		$this->productRepository = $productRepository;
		$this->productFactory = $productFactory;
		$this->storeManager = $storeManager;
		$this->objectManager = $objectManager;
    $this->log = $log;
	}

	/**
	 * Check if extension enabled
	 *
	 * @return string|null
	 */
	public function isEnabled() {
		return $this->scopeConfig->isSetFlag ( self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

	/**
	 * Private Key
	 *
	 * @return string|null
	 */
	public function privateKey() {
		return $this->scopeConfig->getValue ( self::XML_PATH_PRIVATE_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

  /**
	 * Format Membrane Custom Options
	 *
	 * @return array
	 */
  public function formatMembraneCustomOptions($productOptions, $qty){
    $customisedOptions = array();
    $qtyPrefix = "{$qty} x ";
    foreach($productOptions as $option){
      switch($option['label']){
        case 'Terms 1':
        case 'Terms 2':
          // do nothing as we don't want to display these
          break;

        case 'EPDM Area':
          $option['print_value'] = $option['value'] . ' m<sup>2</sup>';
          $option['value'] = $option['print_value'];
          $customisedOptions[] = $option;
          break;

        case 'EPDM Width':
        case 'EPDM Length':
          $option['value'] = str_replace($qtyPrefix, '', $option['value']);
          if(!strpos($option['value'], 'm')){
            $option['value'] = $option['value'] . ' m';
          }
          $customisedOptions[] = $option;
          break;

        default:
          $customisedOptions[] = $option;
      }
    }

    return $customisedOptions;
  }

	/**
	 * Skyseal Product ID
	 *
	 * @return string|null
	 */
	public function skysealProductId($system = null) {
		$productId = null;
		switch ($system) {
			case 'epdm':
				$productId = $this->epdmProductId();
				break;
			case 'grp':
				$productId = $this->grpProductId();
				break;
			case 'flexigrp':
				$productId = $this->flexiGrpProductId();
				break;
		}
		if($productId === null){
			// product not set in the config so create a new one
			$productId = $this->createProduct($system);
		}
		return $productId;
	}

	/**
	 * Skyseal Product EPDM ID
	 *
	 * @return string|null
	 */
	public function epdmProductId() {
		return $this->scopeConfig->getValue ( self::XML_PATH_SKYSEAL_EPDM_PRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

	/**
	 * Skyseal Product GRP ID
	 *
	 * @return string|null
	 */
	public function grpProductId() {
		return $this->scopeConfig->getValue ( self::XML_PATH_SKYSEAL_GRP_PRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

	/**
	 * Skyseal Product FlexiGRP ID
	 *
	 * @return string|null
	 */
	public function flexiGrpProductId() {
		return $this->scopeConfig->getValue ( self::XML_PATH_SKYSEAL_FLEXIGRP_PRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE );
	}

	/**
	* Custom SumIT create product
	*
	* @return string|null
	*/
	private function createProduct($system = null){
		// construct the name and the sku
		$productSku = "customkit_{$system}";
		$productName = ucfirst($system) . ' Custom Kit';
		// check to see if the product exists already
		try {
			$product = $this->productRepository->get($productSku);
			return $product->getId();
		} catch (\Magento\Framework\Exception\NoSuchEntityException $e){
		  // assume the error here is: Requested product doesn't exist. no product found so we need to create it
			// set up the default image for this product
			$defaultImageUrl = "https://www.roofkitbuilder.co.uk/img/kits/{$system}.jpg";
			$defaultImageFileName = basename($defaultImageUrl);
			$mediaFolder = $this->objectManager->get('Magento\Framework\App\Filesystem\DirectoryList')->getPath('media');
			$imageSavePath = "{$mediaFolder}/{$defaultImageFileName}";

			if(!file_exists($imageSavePath)){
				// file isn't already on the server so try and get it from the source URL
		    try {
          $headers = get_headers($defaultImageUrl);
          $headerStatus = $headers[0];
          if(strpos($headerStatus, '404') === FALSE){
            $imageFileContents = file_put_contents($imageSavePath, file_get_contents($defaultImageUrl));
    				if(!$imageFileContents){
    					// if the file is empty, remove the created empty image file
    					unlink($imageSavePath);
    				}
          }else{
            // 404 returned so the image does not exist at source
            $imageSavePath = '';
          }
		    }catch (Exception $e){
					// fail sliently as just have no image on the product
					Mage::log("Error trying to add product image for {$productSku}:  {$e->getMessage()}.", null, 'customsumit_error.log');
		    }
			}
			// create the new product
			$product = $this->productFactory->create();
			$product
				->setStoreId($this->storeManager->getStore()->getId())
				->setWebsiteIds(array($this->storeManager->getStore()->getWebsiteId()))
		    ->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
				->setAttributeSetId($product->getDefaultAttributeSetId())
		    ->setCreatedAt(strtotime('now'))
		    ->setSku($productSku)
		    ->setName($productName)
    		->setWeight(0)
    		->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
    		->setTaxClassId(2)
    		->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE)
    		->setPrice(1.00)
    		->setDescription($productName)
    		->setShortDescription($productName);
      if($imageSavePath != ''){
        $product->addImageToMediaGallery($imageSavePath, array('image', 'thumbnail', 'small_image'), false, true);
      }
      $product->save();
			$product->setStockData(['qty' => 0, 'is_in_stock' => 1, 'manage_stock' => 0, 'use_config_manage_stock' => 0]);
			$product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => 1, 'manage_stock' => 0, 'use_config_manage_stock' => 0]);
			$product->save();
			$newProductId = $product->getId();
			$newProduct = $this->productRepository->getById($newProductId);
			// now the product is created, create the two product options
			$options = [
				[
					'title' => 'Kit reference',
					'type' => 'area',
					'is_require' => 1,
					'sort_order' => 0,
					'price' => 0,
					'price_type' => 'fixed'
				],
				[
						'title' => 'Kit size',
						'type' => 'area',
						'is_require' => 1,
						'sort_order' => 0,
						'price' => 0,
						'price_type' => 'fixed'
				]
			];
			$newProduct->setHasOptions(1);
      $newProduct->setCanSaveCustomOptions(true);
      foreach ($options as $arrayOption) {
        $option = $this->objectManager->create('\Magento\Catalog\Model\Product\Option')
                ->setProductId($newProductId)
                ->setStoreId($this->storeManager->getStore()->getId())
                ->addData($arrayOption);
        $option->save();
        $newProduct->addOption($option);
      }
      $newProduct->save();
			// all complete, return the product
			return $newProductId;
		}
	}
}
