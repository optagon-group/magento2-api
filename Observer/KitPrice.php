<?php
namespace CustomSumit\Api\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\RequestInterface;

/**
 * KitPrice
 *
 * @author jherring
 */
class KitPrice implements ObserverInterface {

	public function __construct(\Psr\Log\LoggerInterface $log, \Magento\Framework\App\RequestInterface $request) {
		$this->log = $log;
    $this->request = $request;
	}

	public function execute(\Magento\Framework\Event\Observer $observer) {
		// Get Event Product Data
		$product = $observer->getEvent()->getData('product');
    $item = $observer->getEvent()->getData('quote_item');
		// If Kit, Update Item Price
		if (!empty($product->kitPrice)) {
			// Item Data
			$item = $observer->getEvent()->getData('quote_item');
			$item = ($item->getParentItem() ? $item->getParentItem() : $item);
			$item->setCustomPrice($product->kitPrice);
			$item->setOriginalCustomPrice($product->kitPrice);
			$item->getProduct()->setIsSuperMode(true);
		}
	}
}
