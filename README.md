# Custom SumIT Kit Builder module for Magento 2

## Installation
```shell
composer require customsumit/magento2-api
```
## Setup
### Settings
* Go to Stores > Settings > Configuration > CUSTOM SUMIT > Kit Builder.
* To enable the module, select Yes from the Enabled dropdown.
* In the Connector Options section, paste Private Key provided Custom SumIT. This must include the header text and footer text on the key:
```
-----BEGIN PRIVATE KEY-----
THD8ikKPWvwIBADqhkiG9w0BAQEFAASCBKkwggSlAgEAA
...
-----END PRIVATE KEY-----
```
* Select Yes from the URL Links Enabled in Admin dropdown to enable Kit Reference URLs in Magento admin to be clickable.
## Requirements
### Magento
* 2.1.x

### PHP
Your website's PHP server must have the following configuration settings. These can be found in the loaded `php.ini` file.
```
allow_url_fopen = On
```
